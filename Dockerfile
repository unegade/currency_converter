FROM python:3.8.3-slim

COPY requirements.txt .
RUN pip install -r requirements.txt

ENV PYTHONPATH "${PYTONPATH}:/app:/"
EXPOSE 8080

COPY app /app
WORKDIR /app

CMD ["python", "main.py"]


