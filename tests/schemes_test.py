from app.schemes import ConvertQuerySchema, ConvertSuccessResponseSchema, DatabaseJsonSchema, DatabaseQuerySchema


def test_ConvertQuerySchema():
    convert_data = {'from': 'USD', 'to': 'RUR', 'amount': 1000.3}
    schema = ConvertQuerySchema().load(convert_data)
    assert schema.get('from_') == convert_data['from']
    assert schema.get('to') == convert_data['to']
    assert schema.get('amount') == convert_data['amount']


def test_DatabaseQuerySchema():
    schema = DatabaseQuerySchema().load({'merge': 1})
    assert schema.get('merge') == 1


def test_DatabaseJsonSchema():
    db_data = {"set": {"USD": 1, "RUR": 69.5725}}
    schema = DatabaseJsonSchema().load(db_data)
    assert schema.get('set').get('USD') == 1


def test_ConvertSuccessResponseSchema():
    schema = ConvertSuccessResponseSchema().load({'result': 1.005})
    assert schema.get('result') == 1.005
