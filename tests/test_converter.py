from unittest.mock import patch

import pytest
from aiohttp.web_exceptions import HTTPNotFound

from app.converter import convert_currency


async def mock_get_currency(_self, key):
    return {'USD': 1, 'RUR': 69.5, 'EUR': 0.89, 'UAH': 26.7289}.get(key)


@patch('app.store.Store.get_currency', mock_get_currency)
@pytest.mark.parametrize("from_, to, amount, expected",
                         [
                             ('RUR', 'USD', 1500, 21.5827),
                             ('USD', 'RUR', 67, 4656.5),
                             ('EUR', 'RUR', 100, 7808.9888),
                             ('UAH', 'EUR', 93, 3.0966),
                         ]
                         )
@pytest.mark.asyncio
async def test_convert_currency(from_, to, amount, expected):
    assert await convert_currency(from_, to, amount) == expected


@patch('app.store.Store.get_currency', mock_get_currency)
@pytest.mark.parametrize("from_, to, amount",
                         [
                             ('QWE', 'USD', 10),
                             ('USD', 'QWE', 10),
                         ]
                         )
@pytest.mark.asyncio
async def test_convert_currency_key_not_found(from_, to, amount):
    with pytest.raises(HTTPNotFound) as excinfo:
        await convert_currency(from_, to, amount)
    error_text = excinfo.value.text
    assert 'Not found' in error_text
    assert 'QWE' in error_text
