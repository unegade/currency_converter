from unittest.mock import patch, AsyncMock

import pytest
from aiohttp.test_utils import make_mocked_coro

@patch('aioredis.create_redis_pool', new_callable=AsyncMock)
@pytest.mark.asyncio
async def test_convert(pool_mock, aiohttp_client, loop):
    with patch('app.converter.convert_currency', make_mocked_coro(1.103)) as mock_convert_currency:
        from app.main import init_app  # TODO: Найти как обходить. Если импортировать в начале, то patch не работает
        client = await aiohttp_client(await init_app(loop))
        resp = await client.get('/convert', params={'from': 'RUR', 'to': 'USD', 'amount': '1000.50'})
        text = await resp.text()
        print(text)
        assert resp.status == 200
        assert '{"result": 1.103}' == text
        assert mock_convert_currency.call_count == 1


@pytest.mark.parametrize("params, status_code, text_in_response",
                         [
                             ({'from': 'RUR', 'to': 'USD', 'amount': '1000.50'}, 200, 'result'),
                             ({'qqqq': 'RUR', 'to': 'USD', 'amount': '1000.50'}, 422, 'Missing data'),
                             ({'from': 'RUR', 'to': 'rrr', 'amount': '1000.50'}, 422, 'Missing data'),
                             ({'from': 'RUR', 'to': 'USD', 'reqwew': '1000.50'}, 422, 'Missing data'),
                             ({'from': 'RUR', 'to': 'USD', 'amount': 'qegfdgw'}, 422, 'Not a valid number'),
                         ]
                         )
@pytest.mark.asyncio
async def test_database_error(params, status_code, text_in_response, aiohttp_client, loop):
    with patch('app.converter.convert_currency', make_mocked_coro(1.103)):
        from app.main import init_app  # TODO: Найти как обходить. Если импортировать в начале, то patch не работает
        client = await aiohttp_client(await init_app(loop))
        resp = await client.post('/convert', params=params)
        text = await resp.text()
        print(text)
        assert resp.status == status_code
        assert text_in_response in text


@patch('aioredis.create_redis_pool', new_callable=AsyncMock)
@pytest.mark.asyncio
async def test_database(pool_mock, aiohttp_client, loop):
    with patch('app.store.Store.set_currencies', make_mocked_coro(None)) as mock_set_currencies:
        from app.main import init_app  # TODO: Найти как обходить. Если импортировать в начале, то patch не работает
        client = await aiohttp_client(await init_app(loop))
        resp = await client.post('/database', params={'merge': 1}, json={"set": {"USD": 1, "RUR": 69.5725}})
        text = await resp.text()
        print(text)
        assert resp.status == 200
        assert '{"result": "data updated"}' == text
        assert mock_set_currencies.call_count == 1

@pytest.mark.parametrize("params, json, status_code, text_in_response",
                         [
                             ({}, {"set": {"USD": 1, "RUR": 69.5725}}, 422, 'Missing data'),
                             ({'merge': 1}, {"set": {"USD": '1w', "RUR": 69.5725}}, 422, 'Not a valid number'),
                             ({'merge': 0}, {"set": {}}, 200, 'updated'),
                             ({'merge': 1}, {}, 422, 'Missing data'),
                             ({'merge': 'qwe'}, {}, 422, 'Not a valid boolean'),
                         ]
                         )
@patch('aioredis.create_redis_pool', new_callable=AsyncMock)
@pytest.mark.asyncio
async def test_database_error(create_pool_mock, params, json, status_code, text_in_response, aiohttp_client, loop):
    from app.main import init_app  # TODO: Найти как обходить. Если импортировать в начале, то patch не работает
    client = await aiohttp_client(await init_app(loop))
    resp = await client.post('/database', params=params, json=json)
    text = await resp.text()
    print(text)
    assert resp.status == status_code
    assert text_in_response in text
