import logging
from typing import Dict, List

import aioredis
from aiohttp.web_exceptions import HTTPNotFound, HTTPInternalServerError
from aioredis import Redis

logger = logging.getLogger(__name__)


class Store():
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        if not self._instance:
            self._redis: Redis = None

    async def set_default_values(self):
        """"""
        if not await self._redis.keys('*'):
            default = {
                "USD": 1,
                "RUR": 69.5725,
                "EUR": 0.8919,
                "BYN": 2.3776,
                "UAH": 26.7289,
                "KZT": 403.9051,
                "TRY": 6.8580,
                "KRW": 1208.1281
            }
            await self.set_currencies(default)

    async def test_connect(self):
        try:
            await self._redis.set('__test__', 'test')
            await self._redis.delete('__test__')
        except Exception as e:
            raise HTTPInternalServerError(text=f'Redis connect error: {str(e)}')

    async def connect(self, host: str = 'localhost', port: int = 6379):
        logger.info(f'Connecting to redis {host}:{port}')
        try:
            self._redis = await aioredis.create_redis_pool(f'redis://{host}:{port}')
        except Exception as e:
            raise HTTPInternalServerError(text=f'Redis connect error: {str(e)}')
        logger.info('Connected')
        return self

    async def close(self):
        self._redis.close()
        await self._redis.wait_closed()

    async def _delete_currencies(self, currencies: List[str]):
        """Удаление валют из redis"""
        logger.info(f'Removing currencies from store: {", ".join(currencies)}')
        await self._redis.delete(*currencies)
        logger.info('Removing completed')

    async def set_currencies(self, currencies: Dict[str, float], merge: bool = True):
        """Задать значения валют"""
        if currencies:
            logger.info(f'Set up new value for currencies: {", ".join(currencies)}')
            await self._redis.mset(currencies)
            logger.info(f'Set up new value completed: {", ".join(currencies)}')
        if not merge:  # Удаление валют, отсутствующих в словаре
            all_old_currencies = await self._redis.keys(pattern='*', encoding='utf-8')
            currencies_for_delete = set(all_old_currencies) - set(currencies.keys())
            if currencies_for_delete:
                await self._delete_currencies(currencies_for_delete)

    async def get_currency(self, currency) -> float:
        """Получить значение валюты"""
        if result := await self._redis.get(currency, encoding='utf-8'):
            return float(result)
        else:
            raise HTTPNotFound(text=f'В хранилище отсутствует валюта {currency}')

    async def get_all_currencies(self) -> dict:
        """Получить список всех """
        all_currencies = await self._redis.keys('*', encoding='utf-8')
        return {currency: await self.get_currency(currency) for currency in all_currencies}
