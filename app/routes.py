import logging

from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import json_response
from aiohttp_apispec import (
    docs,
    querystring_schema, json_schema, )

from app.store import Store
from app.converter import convert_currency
from app.schemes import ConvertQuerySchema, DatabaseJsonSchema, DatabaseQuerySchema, ConvertSuccessResponseSchema

logger = logging.getLogger(__name__)
routes = web.RouteTableDef()


@docs(
    summary="Конвертация валют",
    description="",
    responses={
        200: {"description": "Результат конвертации", "schema": ConvertSuccessResponseSchema},
        404: {"description": "Одна из валют не найдена"},
        422: {"description": "Ошибка валидации входных данных"},
        500: {"description": "Ошибка сервера"},
    },
)
@querystring_schema(ConvertQuerySchema)
@routes.get('/convert')
async def convert(request: Request):
    from_ = request['querystring'].get('from')
    to = request['querystring'].get('to')
    amount = request['querystring'].get('amount')
    result = await convert_currency(from_, to, float(amount))
    return json_response({'result': result})


@docs(
    summary="Обновление данных в хранишище",
    description="Курс валют задается относительно доллара(USD)",
    responses={
        200: {"description": "Данные обновлены", "schema": ConvertSuccessResponseSchema},
        422: {"description": "Ошибка валидации входных данных"},
        500: {"description": "Ошибка сервера"},
    },
)
@json_schema(DatabaseJsonSchema)
@querystring_schema(DatabaseQuerySchema)
@routes.post('/database')
async def database(request: Request):
    """Обновление данных по валютам"""
    merge = request['querystring'].get('merge')
    logger.info(f'Updating currencies. Old data will be {"save" if merge else "invalid"}.')
    new_currencies = request['json'].get('set')
    await Store().set_currencies(new_currencies, merge)
    return json_response({'result': 'data updated'})


@docs(
    summary="Получить данные по валютам",
    description="Курс валют отображается относительно доллара(USD)",
    responses={
        200: {"description": "Данные получены"},
        500: {"description": "Ошибка сервера"},
    },
)
@routes.get('/currency')
async def all_currencies(request: Request):
    return json_response(await Store().get_all_currencies())
