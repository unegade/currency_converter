from marshmallow import Schema, fields
from marshmallow.validate import Range


class ConvertQuerySchema(Schema):
    from_ = fields.Str(description="Из какой валюты конвертировать", required=True, data_key='from')
    to = fields.Str(description="В какую валюту конвертировать", required=True)
    amount = fields.Float(description="Сумма для конвертации", required=True,
                          validate=Range(min=0.001, error="Значение должно быть больше 0"))


class DatabaseQuerySchema(Schema):
    merge = fields.Bool(description="Если 'true' - данные будут объеденены, "
                                    "если 'false' - старые данные будут удалены",
                        required=True)


class DatabaseJsonSchema(Schema):
    set = fields.Dict(description="Список новых данных", key=fields.Str(), values=fields.Float(), required=True)


class ConvertSuccessResponseSchema(Schema):
    result = fields.Float()
