import logging.config

from aiohttp import web
from aiohttp.abc import Application
from aiohttp.web_exceptions import HTTPInternalServerError
from aiohttp_apispec import validation_middleware, setup_aiohttp_apispec
from dynaconf import settings

from app.routes import routes
from app.store import Store

logging.config.dictConfig(settings.LOGGER_CONFIG)
logger = logging.getLogger(__name__)


async def on_startup(app: Application):
    logger.info('Startup web server')
    store = await Store().connect(settings.REDIS.HOST, settings.REDIS.PORT)
    await store.test_connect()
    await store.set_default_values()


async def on_shutdown(app: Application):
    logger.info('Shutdown web server')
    await Store().close()


async def init_app(loop=None) -> Application:
    @web.middleware
    async def error_middleware(request, handler):
        try:
            response = await handler(request)
            return response
        except web.HTTPException as ex:
            logger.error(str(ex))
            raise
        except Exception as ex:
            logger.critical(str(), exc_info=True)
            raise HTTPInternalServerError(text=str(ex))

    app = web.Application(loop=loop, middlewares=[error_middleware, validation_middleware])
    app.add_routes(routes)
    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)
    setup_aiohttp_apispec(
        app=app,
        title="My Documentation",
        version="v1",
        url="/docs/swagger.json",
        swagger_path="/docs",
    )
    return app


if __name__ == '__main__':
    web.run_app(init_app(), **settings.AIOHTTP)
