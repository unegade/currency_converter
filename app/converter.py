from aiohttp.web_exceptions import HTTPNotFound

from app.store import Store


async def convert_currency(from_: str, to: str, amount: float) -> float:
    """Конвертация валюты"""
    store = Store()
    if not (from_val := await store.get_currency(from_)):
        raise HTTPNotFound(text=f'Not found info for {from_} in store')
    if not (to_val := await store.get_currency(to)):
        raise HTTPNotFound(text=f'Not found info for {to} in store')
    result = round(amount / from_val * to_val, 4)
    return result
